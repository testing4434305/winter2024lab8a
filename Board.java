public class Board{
	//field
	private Tile[][] grid;
	//construcor
	public Board(){
		int size = 3;
		int index = 0;
		this.grid = new Tile[size][size];
		for(int i=0; i<grid.length; i++){
			for(int j=0; j<grid[i].length; j++){
				grid[i][j]=Tile.BLANK;
			}
		}
	}
	public String toString(){
		String boardRow = "";
		for(Tile[] arrays:this.grid){
			for(Tile value:arrays){
				boardRow+=value.getName();
				boardRow+=" ";
			}
			boardRow+="\n";
		}
		return boardRow;
	}
	public boolean placeToken(int row, int col, Tile playerToken){
		Tile position = Tile.BLANK;
		if(row>3 || row<1){
			return false;
		}
		if(col>3 || col<1){
			return false;
		}
		if(this.grid[row-1][col-1] == position){
			this.grid[row-1][col-1] = playerToken;
			return true;
		}
		else{
			return false;
		}
	}
	public boolean checkIfFull(){
		for(Tile[] arrays:this.grid){
			for(Tile value:arrays){
				if(value == Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Tile playerToken){
		int j=0;
		for(int i=0; i<this.grid.length; i++){
			if(this.grid[i][j] == playerToken && this.grid[i][j] == this.grid[i][j+1] && this.grid[i][j+1] == this.grid[i][j+2]){
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningfVertical(Tile playerToken){
		int j=0;
		for(int i=0; i<this.grid.length; i++){
			if(this.grid[j][i] == playerToken && this.grid[j][i] == this.grid[j+1][i] && this.grid[j+1][i] == this.grid[j+2][i]){
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningDiagonal(Tile playerToken){
		int j=0;
		int i=0;
			if(this.grid[i][j] == playerToken && this.grid[i][j] == this.grid[i+1][j+1] && this.grid[i+1][j+1] == this.grid[i+2][j+2]){
				return true;
			}else if(this.grid[i][j+2] == playerToken && this.grid[i][j+2] == this.grid[i+1][j+1] && this.grid[i+1][j+1] == this.grid[i+2][j]){
				return true;
			}
		return false;
	}
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningfVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}else{
			return false;
		}
	}
}