public enum Tile{
	BLANK("_"),
	X("X"),
	O("O");
	//field
	private String name;
	
	//construcor
	private Tile(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
}