import java.util.Scanner;
public class TicTacToeApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello, Let's play the Tic-Tac-Toe Game!\nPlayer 1 has X\nPlayer 2 has O");
		Board game = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		while(!gameOver){
			System.out.println(game.toString());
			System.out.println("Enter a row and a column for the position you want to place your token: ");
			int row = reader.nextInt();
			int col = reader.nextInt();
			if(player == 1){
				playerToken = Tile.X;
			}else{
				playerToken = Tile.O;
			}
			while(!game.placeToken(row,col,playerToken)){
				System.out.println("Enter a valid row and column");
				row = reader.nextInt();
				col = reader.nextInt();
			}
			if(game.checkIfWinning(playerToken)){
				System.out.println("Player "+ player +" wins!");
				gameOver = true;
			}else if(game.checkIfFull()){
				System.out.println("It's a tie!");
				gameOver = true;
			}else if(player == 1){
					player = 2;
				}else{
					player =1;
				}
	}
}
}